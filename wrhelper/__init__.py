from datetime import datetime
from timezones import UTC
import hashlib
import re


def convert_to_utc(local_datetime, local_timezone):
    """
    Convert a datetime string like '2015-12-15T18:00:00+08:00' or just '2015-12-15T18:00:00' to UTC.
    :param local_datetime: str
    :param local_timezone: datetime.tzinfo
    :return: str
    """
    datetime_format = '%Y-%m-%dT%H:%M:%S'
    normalized_datetime_str = normalize_datetime_str(local_datetime)

    if normalized_datetime_str is None:
        naive_datetime = datetime.strptime(local_datetime, datetime_format)
    else:
        naive_datetime = datetime.strptime(normalized_datetime_str, datetime_format)

    local_datetime = naive_datetime.replace(tzinfo=local_timezone)
    utc_datetime = local_datetime.astimezone(UTC())

    return utc_datetime.strftime('%Y-%m-%dT%H:%M:%S%z')


def get_now():
    return datetime.now(UTC()).strftime('%Y-%m-%dT%H:%M:%S%z')


def get_md5_hash(path_to_file):
    m = hashlib.md5()
    with open(path_to_file, mode='rb') as f:
        m.update(f.read())
    return m.hexdigest()


def normalize_datetime_str(datetime_str):
    """
    Normalize an ISO 8601 string. For example, '2015-12-15T18:00:00+08:00' will be '2015-12-15T18:00:00'.
    :param datetime_str: str
    :return: str | None
    """
    pattern = r'([\s\S]+)([+-]\d{2}:\d{2})'

    match = re.match(pattern, datetime_str)
    if match:
        return match.group(1)
