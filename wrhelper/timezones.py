from datetime import timedelta, tzinfo


class UTC(tzinfo):
    def dst(self, dt):
        return timedelta(0)

    def utcoffset(self, dt):
        return timedelta(0)

    def tzname(self, dt):
        return 'UTC'


class GMT8(tzinfo):
    def dst(self, dt):
        return timedelta(0)

    def utcoffset(self, dt):
        return timedelta(hours=8) + self.dst(dt)

    def tzname(self, dt):
        return 'GMT +8'


class GMT9(tzinfo):
    def dst(self, dt):
        return timedelta(0)

    def utcoffset(self, dt):
        return timedelta(hours=9) + self.dst(dt)

    def tzname(self, dt):
        return 'GMT +9'